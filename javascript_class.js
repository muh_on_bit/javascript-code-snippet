/**
 * Slide Tabs/UI Tabs
 */
window.SlideTabs = {};
( function( window, $, that ) {

    // Constructor
    that.init = function() {
        that.cache();

        if ( that.meetsRequirements ) {
            that.bindEvents();
        }
    };

    // Cache all the things
    that.cache = function() {
        that.$c = {
            window: $(window),
            tabsModule: $( '.content-tabs-module' ),
            tabsHeader: $( '.content-tabs-header' ),
            singleTabHeader: $( '.content-tabs-header li' ),
            activeTabHeader: $( '.active-tab' ),
            tabsGoButton: $( '.content-tabs-button' ),
        };
    };

    // Combine all events
    that.bindEvents = function() {

        // Initiate tabs
        that.initiateTabs();

        // Find the active tab, if one is forced
        that.findActiveTab();

        // Special functionality just for mobile
        if ( that.$c.window.width() < 768 ) {

            // Set the URL for the button to the active tab
            that.setButtonURL();

            // Listen for clicks
            $( 'body' ).on( 'click', '.content-tabs-header li', that.showHideTabHeadings );
        }
    };

    // Do we meet the requirements?
    that.meetsRequirements = function() {
        return that.$c.tabsModule.length;
    };

    // Find the active tab is one is forced
    that.findActiveTab = function() {

        var activeTab = that.$c.tabsModule.find( 'li.active-tab' );

        if ( activeTab.length ) {
        	return activeTab.index();
        }

        return false;
    };

    // Set the button URL to the active tab
    that.setButtonURL = function() {

        // Get the active tab for this instance of tabs
        var activeTabURL = that.$c.tabsGoButton.parents( '.content-tabs-module' ).find( '.active-tab a' ).attr( 'data-url' );

        that.$c.tabsGoButton.attr( 'href', activeTabURL );
    }

    // Initiate the tabs
    that.initiateTabs = function() {

        that.$c.tabsModule.tabs({
			active: that.findActiveTab(),
			beforeActivate: function(e, ui) {
				if( $(ui.newTab).find('a').data('url').indexOf('#') != 0 ) {

                    // Don't open links on mobile
                    if ( that.$c.window.width() >= 768 ) {
    					window.open($(ui.newTab).find('a').data('url'), '_self');
                    }
				}
			}
		});
    };

    // Show/Hide tab headings when clicked on mobile
    that.showHideTabHeadings = function() {

        // We don't need to click the active tab link since we're already on the page
        event.preventDefault();

        // Get the URL for this tab link
        var tabURL = $( this ).find( 'a' ).attr( 'data-url' );

        // Set the URL in the Go button
        if ( ! $( this ).hasClass( 'active-tab' ) ) {
            $( this ).parents( '.content-module' ).find( '.content-tabs-button' ).attr( 'href', tabURL ).addClass( 'active-button' );
        }

        // Toggle a class on the parent container
        $( this ).parents( '.content-header' ).toggleClass( 'show-hidden-tabs' );

        // Set the active tab class
        that.$c.singleTabHeader.removeClass( 'active-tab' );
        $( this ).addClass( 'active-tab' );
    }

    // Engage
    $( that.init );

})( window, jQuery, window.SlideTabs );